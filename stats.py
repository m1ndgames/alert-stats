import os
import shutil
from collections import Counter
from zipfile import ZipFile
from flask import Flask, render_template, abort, request
import sqlite3
import json
import requests
import time
from calendar import timegm
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-a", "--apikey", help="AI-Arena Api Key")
parser.add_argument("-b", "--botid", help="AI-Arena bot ID")
args = parser.parse_args()
app = Flask(__name__)
app.jinja_env.add_extension('jinja2.ext.loopcontrols')
app.config.from_object(__name__)

if args.apikey:
    aiarena_api_key = args.apikey
else:
    print("You need to supply an API key")
    exit

if args.botid:
    my_bot_id = args.botid
else:
    print("You need to supply a bot ID")
    exit

sql_elo_table = """ CREATE TABLE IF NOT EXISTS match_history (
                                        id INTEGER PRIMARY KEY,
                                        match int,
                                        elo int,
                                        date int
                                    ); """


def setup_database():
    """ create a database connection to a SQLite database """
    db = sqlite3.connect(r"data/match_data.sqlite")
    try:
        print("Connected to sqlite db v." + str(sqlite3.version))
        create_table(db, sql_elo_table)
    except sqlite3.Error as e:
        print(e)
    finally:
        if db:
            db.close()


def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except sqlite3.Error as e:
        print(e)


def get_match_date(matchid=None):
    r = requests.get(f"https://ai-arena.net/api/matches/" + str(matchid) + "/",
                     headers={'Authorization': "Token " + aiarena_api_key})
    d = json.loads(r.text)
    if d:
        if 'started' in d:
            datestring = d['started']
            utc_time = time.strptime(datestring, "%Y-%m-%dT%H:%M:%S.%fZ")
            epoch_time = timegm(utc_time)
            return str(epoch_time)


@app.route("/update_match_history")
def update_match_history():
    # Get the last Match id from database
    db = sqlite3.connect(r"data/match_data.sqlite")
    cursor = db.cursor()
    sqlite_select_query = """SELECT match from match_history ORDER BY match DESC Limit 1"""
    cursor.execute(sqlite_select_query)
    records = cursor.fetchall()

    last_match_id = None
    if records:
        last_match_id = records[0][0]
    db.close()

    # Query the API
    if last_match_id:
        # Request only new data
        print("Importing new data, starting at " + str(last_match_id))
        r = requests.get(
            f"https://ai-arena.net/api/match-participations/?bot=" + str(
                my_bot_id) + "&ordering=id&limit=10000000&min_match=" + str(
                last_match_id + 1), timeout=600,
            headers={'Authorization': "Token " + aiarena_api_key})
    else:
        # Request all
        print("Importing Data")
        r = requests.get(
            f"https://ai-arena.net/api/match-participations/?bot=" + str(my_bot_id) + "&ordering=id&limit=10000000",
            timeout=600,
            headers={'Authorization': "Token " + aiarena_api_key})

    if r:
        d = json.loads(r.text)
        if d:
            data = d['results']
            db = sqlite3.connect(r"data/match_data.sqlite")
            cursor = db.cursor()

            count = 0
            for dataline in data:
                match_id = dataline['match']
                new_elo = dataline['resultant_elo']
                if new_elo:
                    match_date = get_match_date(match_id)
                    if match_date:
                        sqlite_insert_with_param = """INSERT INTO 'match_history'
                                                      ('match', 'elo', 'date')
                                                      VALUES (?, ?, ?);"""

                        data_tuple = (match_id, new_elo, match_date)
                        cursor.execute(sqlite_insert_with_param, data_tuple)
                        db.commit()
                        print("Added match " + str(match_id) + " to Database")
                        count = count + 1

            db.close()

            return render_template('import_match_history.html', count=count)

    else:
        abort(404)


@app.route("/import_database")
def import_database():
    # Get the last Match id from database
    r = requests.get(f"https://ai-arena.net/api/bots/" + str(my_bot_id) + "/data/",
                     headers={'Authorization': "Token " + aiarena_api_key})
    if r:
        open('data.zip', 'wb').write(r.content)

        if os.path.isfile('data/data.sqlite'):
            os.remove("data/data.sqlite")

        with ZipFile('data.zip', 'r') as zipObj:
            # Get a list of all archived file names from the zip
            listOfFileNames = zipObj.namelist()
            # Iterate over the file names
            for fileName in listOfFileNames:
                # Check filename endswith csv
                if fileName == 'data.sqlite':
                    # Extract a single file from zip
                    zipObj.extract(fileName, './')

        shutil.move("data.sqlite", "data/data.sqlite")
        os.remove("data.zip")

        return render_template('import_database.html')

    else:
        abort(404)


@app.route("/match_history.json")
def match_history_json():
    connection = sqlite3.connect("data/match_data.sqlite")
    cursor = connection.cursor()
    cursor.execute("SELECT * from match_history ORDER BY date ASC")
    results = cursor.fetchall()
    if results:
        result_data = []
        for r in results:
            elo = r[2]
            date = r[3] * 1000
            result_data.append([date, elo])
        return json.dumps(result_data, sort_keys=True, indent=2)
    else:
        abort(404)


@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html', title='404'), 404


@app.route("/strategies.json")
def get_strategies_json():
    my_race = request.args.get('my_race', type=str)
    enemy_race = request.args.get('enemy_race', type=str)
    chart_type = request.args.get('chart_type', type=str)
    enemy_bot_id = request.args.get('enemy_bot_id', type=str)

    connection = sqlite3.connect("data/data.sqlite")
    cursor = connection.cursor()

    # Global strategy stats
    if my_race == 'null' and enemy_race == 'null' and enemy_bot_id == 'null':
        cursor.execute("SELECT my_strategy from games WHERE result = \"Result.Victory\"")
    # Race specific strategy stats
    elif my_race != 'null' and enemy_race == 'null' and enemy_bot_id == 'null':
        cursor.execute("SELECT my_strategy from games WHERE my_race = ? AND result = \"Result.Victory\"", (my_race,))
    # Race specific vs Enemy race specific strategy stats
    elif my_race != 'null' and enemy_race != 'null' and enemy_bot_id == 'null':
        cursor.execute("SELECT my_strategy from games WHERE my_race = ? AND enemy_race = ? AND result = \"Result.Victory\"", (my_race, enemy_race,))
    # Race specific vs specific enemy
    elif my_race != 'null' and enemy_bot_id != 'null':
        cursor.execute("SELECT my_strategy from games WHERE my_race = ? AND enemy_bot_id = ? AND result = \"Result.Victory\"", (my_race, enemy_bot_id,))

    results = cursor.fetchall()
    if results:
        strategy_data = []
        for r in results:
            strategy = str(r[0])
            strategy_data.append(str(strategy))

        result = Counter(strategy_data).most_common()
        return json.dumps(result, sort_keys=True, indent=4)
    else:
        abort(404)

@app.route("/unitlist.json")
def get_unitlist_json():
    my_race = request.args.get('my_race', type=str)
    enemy_race = request.args.get('enemy_race', type=str)
    enemy_bot_id = request.args.get('enemy_bot_id', type=str)

    connection = sqlite3.connect("data/data.sqlite")
    cursor = connection.cursor()

    # Race specific unit list per bot
    if my_race != 'null' and enemy_race != 'null' and enemy_bot_id != 'null':
        cursor.execute("SELECT enemy_units from enemy_unit_compositions WHERE my_race = ? AND enemy_race = ? AND enemy_bot_id = ?", (my_race, enemy_race, enemy_bot_id,))

    results = cursor.fetchall()
    if results:
        unit_data = []
        for r in results:
            unitstring = str(r[0])
            units = unitstring.split(";")
            for unit in units:
                if unit != "":
                    unit_data.append(str(unit))

        result = Counter(unit_data).most_common()
        return json.dumps(result, sort_keys=True, indent=4)
    else:
        abort(404)

@app.route("/results.json")
def get_results_json():
    my_race = request.args.get('my_race', type=str)
    enemy_race = request.args.get('enemy_race', type=str)
    chart_type = request.args.get('chart_type', type=str)
    enemy_bot_id = request.args.get('enemy_bot_id', type=str)

    connection = sqlite3.connect("data/data.sqlite")
    cursor = connection.cursor()

    if chart_type == 'winloss':
        # Global Stats
        if my_race == 'null' and enemy_race == 'null' and enemy_bot_id == 'null':
            cursor.execute("SELECT result from games")

        # Stats per Race
        if my_race != 'null' and enemy_race == 'null' and enemy_bot_id == 'null':
            cursor.execute('SELECT result from games WHERE my_race = ?', (my_race,))

        # Stats per Race vs Enemy Race
        elif my_race != 'null' and enemy_race != 'null' and enemy_bot_id == 'null':
            cursor.execute(
                'SELECT result from games WHERE my_race = ? AND enemy_race = ?', (my_race, enemy_race,))

        # Stats vs Opponent
        elif my_race == 'null' and enemy_bot_id != 'null':
            cursor.execute(
                'SELECT result from games WHERE enemy_bot_id = ?', (enemy_bot_id,))

        # Stats per Race vs Opponent
        elif my_race != 'null' and enemy_bot_id != 'null':
            cursor.execute(
                'SELECT result from games WHERE my_race = ? AND enemy_bot_id = ?', (my_race, enemy_bot_id,))

    results = cursor.fetchall()
    win_count = 0
    loss_count = 0
    tie_count = 0
    if results:
        for r in results:
            if str(list(r)[0]) == 'Result.Defeat':
                loss_count = loss_count + 1
            elif str(list(r)[0]) == 'Result.Victory':
                win_count = win_count + 1
            elif str(list(r)[0]) == 'Result.Tie':
                tie_count = tie_count + 1

        result_data = [{'name': 'Win', 'y': win_count, 'selected': 'True'}, {'name': 'Loss', 'y': loss_count},
                       {'name': 'Tie', 'y': tie_count}]
        return json.dumps(result_data, sort_keys=True, indent=4)
    else:
        abort(404)


def read_rounds_data(enemy_bot_id=None):
    if not os.path.isfile('data/data.sqlite'):
        import_database()
        return

    connection = sqlite3.connect("data/data.sqlite")
    cursor = connection.cursor()
    if enemy_bot_id:
        cursor.execute('SELECT * from games WHERE enemy_bot_id = ? ORDER BY id DESC', (enemy_bot_id,))
    else:
        cursor.execute("SELECT * from games ORDER BY id DESC")
    results = cursor.fetchall()
    if results:
        return results


@app.route("/")
def index():
    game_data = read_rounds_data()
    return render_template('index.html', game_data=game_data)


@app.route('/enemy')
def enemy_stats():
    enemy_bot_id = request.args.get('enemy_bot_id', type=str)
    game_data = read_rounds_data(str(enemy_bot_id))
    win_strategy_terran = db_calculate_win_strategy(enemy_bot_id, 'Race.Terran')
    win_strategy_protoss = db_calculate_win_strategy(enemy_bot_id, 'Race.Protoss')
    win_strategy_zerg = db_calculate_win_strategy(enemy_bot_id, 'Race.Zerg')
    return render_template('enemy.html', game_data=game_data, enemy_bot_id=enemy_bot_id, win_strategy=[win_strategy_terran, win_strategy_protoss, win_strategy_zerg])


@app.route('/piechart')
def piechart():
    chart_type = request.args.get('chart_type', type=str)
    my_race = request.args.get('my_race', type=str)
    enemy_race = request.args.get('enemy_race', type=str)
    enemy_bot_id = request.args.get('enemy_bot_id', type=str)
    return render_template('piechart.html', chart_type=chart_type, my_race=my_race, enemy_race=enemy_race, enemy_bot_id=enemy_bot_id)

@app.route('/unitlist')
def unitlist_chart():
    my_race = request.args.get('my_race', type=str)
    enemy_race = request.args.get('enemy_race', type=str)
    enemy_bot_id = request.args.get('enemy_bot_id', type=str)
    return render_template('unitlist.html', my_race=my_race, enemy_race=enemy_race, enemy_bot_id=enemy_bot_id)


def db_calculate_win_strategy(enemy_bot_id=None, my_race=None):
    """ create a database connection to a SQLite database """
    db = None
    try:
        if enemy_bot_id:
            db = sqlite3.connect(r"data/data.sqlite")
            cursor = db.cursor()
            sqlite_select_query = """SELECT my_strategy, count(my_strategy) as win_count from games where enemy_bot_id = ? AND my_race = ? AND result = ? group by my_strategy order by my_strategy desc"""
            cursor.execute(sqlite_select_query, (str(enemy_bot_id), str(my_race), 'Result.Victory'))
            records = cursor.fetchall()

            if records:
                return records

    except sqlite3.Error as e:
        print(e)
    finally:
        if db:
            db.close()


if __name__ == "__main__":
    if not os.path.isdir("data"):
        os.mkdir("data")

    if not os.path.isfile('data/match_data.sqlite'):
        setup_database()
        update_match_history()

    app.run(host='0.0.0.0', port=5000, debug=True)
