var chart;

$(document).ready(function() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const my_race = urlParams.get('my_race');
    const enemy_race = urlParams.get('enemy_race');
    const chart_type = urlParams.get('chart_type');
    const enemy_bot_id = urlParams.get('enemy_bot_id');

    if (chart_type == 'winloss') {
        jsonpath = 'results.json'
        if (my_race == 'Race.Terran') {
            title_shown_race = 'Terran'
        } else if (my_race == 'Race.Protoss') {
            title_shown_race = 'Protoss'
        } else if (my_race == 'Race.Zerg') {
            title_shown_race = 'Zerg'
        } else {
            title_shown_race = 'Global'
        }

        if (enemy_race == 'Race.Terran') {
            title_shown_enemy_race = 'Terran'
        } else if (enemy_race == 'Race.Protoss') {
            title_shown_enemy_race = 'Protoss'
        } else if (enemy_race == 'Race.Zerg') {
            title_shown_enemy_race = 'Zerg'
        } else {
            title_shown_enemy_race = 'Global'
        }

        if (enemy_race != null) {
            chart_title = title_shown_race + " vs " + title_shown_enemy_race + " Win/Loss/Tie Ratio"
        } else if (enemy_race == null) {
            chart_title = title_shown_race + " Win/Loss/Tie Ratio"
        }

    } else if (chart_type == 'winning_strategy') {
        jsonpath = 'strategies.json'
        if (my_race == 'Race.Terran') {
            title_shown_race = 'Terran'
        } else if (my_race == 'Race.Protoss') {
            title_shown_race = 'Protoss'
        } else if (my_race == 'Race.Zerg') {
            title_shown_race = 'Zerg'
        } else {
            title_shown_race = 'Global'
        }

        if (enemy_race == 'Race.Terran') {
            title_shown_enemy_race = 'Terran'
        } else if (enemy_race == 'Race.Protoss') {
            title_shown_enemy_race = 'Protoss'
        } else if (enemy_race == 'Race.Zerg') {
            title_shown_enemy_race = 'Zerg'
        } else {
            title_shown_enemy_race = 'Global'
        }

        if (enemy_race != null) {
            chart_title = title_shown_race + " vs " + title_shown_enemy_race + " Strategy Win Ratio"
        } else if (enemy_race == null) {
            chart_title = title_shown_race + " Strategy Win Ratio"
        }
    }

    $.getJSON("/" + jsonpath + "?my_race=" + my_race + "&enemy_race=" + enemy_race + "&chart_type=" + chart_type + "&enemy_bot_id=" + enemy_bot_id, function(data) {
        chart = new Highcharts.chart('piechart', {

            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                backgroundColor: 'rgba(0,0,0,0)',
                spacingBottom: 10,
                spacingTop: 10,
                spacingLeft: 10,
                spacingRight: 10
            },
            title: {
                style: { "color": "#86c232", "fontSize": "15px", "font-family": "Gugi, cursive" },
                text: chart_title
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    borderColor: "#FFFFFF",
                    borderWidth: "0",
                    minSize: "50",
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: { "color": "#FFFFFF", "text-shadow": "3px 3px 4px rgba(134,194,50,0.5)", "fontSize": "10px", "font-family": "Quicksand" }
                    }
                }
            },
            series: [{
                name: '%',
                colorByPoint: true,
                data: data
            }]
        })
    })
})