var chart;

$(document).ready(function() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const my_race = urlParams.get('my_race');
    const enemy_race = urlParams.get('enemy_race');
    const enemy_bot_id = urlParams.get('enemy_bot_id');

    if (my_race == 'Race.Terran') {
        title_shown_race = 'Terran'
    } else if (my_race == 'Race.Protoss') {
        title_shown_race = 'Protoss'
    } else if (my_race == 'Race.Zerg') {
        title_shown_race = 'Zerg'
    }
    if (enemy_race == 'Race.Terran') {
        title_shown_enemy_race = 'Terran'
    } else if (enemy_race == 'Race.Protoss') {
        title_shown_enemy_race = 'Protoss'
    } else if (enemy_race == 'Race.Zerg') {
        title_shown_enemy_race = 'Zerg'
    }

    chart_title = title_shown_race + " vs " + title_shown_enemy_race

    $.getJSON("/unitlist.json?my_race=" + my_race + "&enemy_race=" + enemy_race + "&enemy_bot_id=" + enemy_bot_id, function(data) {
        chart = new Highcharts.chart('unit_distribution', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                backgroundColor: 'rgba(0,0,0,0)',
                width: 480,
                height: 400,
                spacingBottom: 10,
                spacingTop: 10,
                spacingLeft: 30,
                spacingRight: 10
            },
            title: {
                text: chart_title,
                align: 'center',
                verticalAlign: 'middle',
                y: 110,
                style: { "color": "#86c232", "fontSize": "15px", "font-family": "Gugi, cursive" }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        distance: -50,
                        style: { "color": "#FFFFFF", "text-shadow": "3px 3px 4px rgba(134,194,50,0.5)", "fontSize": "10px", "font-family": "Quicksand" }
                    },
                    startAngle: -90,
                    endAngle: 90,
                    center: ['50%', '75%'],
                    size: '110%',
                    allowPointSelect: true,
                    borderColor: "#FFFFFF",
                    borderWidth: "0",
                    minSize: "50",
                    cursor: 'pointer'
                }
            },
            series: [{
                type: 'pie',
                name: 'Usage',
                innerSize: '50%',
                data: data
            }]
        }
    );
    });
});