$(document).ready(function() {
    resolution = screen.width - 300

    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const enemy_bot_id = urlParams.get('enemy_bot_id');
    $.getJSON("/match_history.json", function(data) {

    Highcharts.chart('elo_chart', {

        chart: {
            zoomType: 'x',
            backgroundColor: 'rgba(0,0,0,0.1)',
            width: resolution,
            height: 350,
            spacingBottom: 10,
            spacingTop: 10,
            spacingLeft: 10,
            spacingRight: 10
        },

        title: {
            style: { "color": "#86c232", "fontSize": "15px", "font-family": "Gugi, cursive" },
            text: ''
        },

        tooltip: {
            valueDecimals: 0
        },

        xAxis: {
            type: 'datetime',
        },

        yAxis: {
            gridLineWidth: 0,
            title: ''
        },

        navigation: {
            buttonOptions: {
                enabled: false
            }
        },

        series: [{
            data: data,
            fillColor: '#86c232',
            pointWidth: 50,
            pointPadding: 0.25,
            borderWidth: 0,
            borderColor: 'transparent',
            lineWidth: 0.25,
            lineColor: '#86c232',
            name: 'ELO',
            showInLegend: false
        }]

    })})
})