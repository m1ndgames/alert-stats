# A.L.E.R.T - Stats


Run the stats.py with the following parameters:
``python stats.py --apikey=<yourapikey> --botid=<yourbotid>``

Initially the script will fetch the match history from the ai-arena api, when its done you can access the stats on http://0.0.0.0:5000

Demo: https://alert.ai-arena.net/
